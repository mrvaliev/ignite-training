package com.amrusoft.valiev.service;

import com.amrusoft.valiev.model.*;
import com.amrusoft.valiev.model.Transaction;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.transactions.*;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

/**
 * Created by Айрат on 30.08.2016.
 */
public class TransferService {


    private IgniteCache igniteCache;


    public TransferService(IgniteCache<AccountKey, Account> igniteCache) {
        this.igniteCache = igniteCache;
    }


    public void makeTransfer(Account credit, Account debit, long sum) {

        Thread thread=new Thread();

        Account accountDB = getAccount(debit.getId());
        Account accountCR = getAccount(credit.getId());

        BigDecimal summ = BigDecimal.valueOf(sum);

        Transaction transaction = new Transaction();
        transaction.setCredit(credit.getId());
        transaction.setDebit(debit.getId());
        transaction.setSum(BigDecimal.valueOf(sum));

        Timestamp timestamp = Timestamp.from(Instant.now());
        transaction.setId(timestamp);

        accountCR.setBalance(accountCR.getBalance().add(summ.negate()));
        accountDB.setBalance(accountDB.getBalance().add(summ));

        igniteCache.put(new TransactionKey(timestamp), transaction);
        putAccount(accountCR);
        putAccount(accountDB);
    }



    public void makeTransferAsync(Account credit, Account debit, long sum){
        Thread thread=new Thread(()->makeTransfer(credit,debit,sum));
        thread.start();
    }


    private Account getAccount(long id) {
        return (Account) igniteCache.get(new AccountKey(id));
    }


    private void putAccount(Account account) {
        igniteCache.put(new AccountKey(account.getId()), account);
    }


    public static class TransactionTask implements Runnable {
        @Override
        public void run() {

        }
    }
}
