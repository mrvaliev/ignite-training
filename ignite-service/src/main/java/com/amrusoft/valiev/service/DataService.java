package com.amrusoft.valiev.service;

import com.amrusoft.valiev.model.Account;
import com.amrusoft.valiev.model.AccountKey;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.transactions.Transaction;

import java.math.BigDecimal;


/**
 * Created by Айрат on 22.08.2016.
 */
public class DataService {


    public void testAccounts() {
        try (Ignite ignite = Ignition.start("example-ignite.xml")) {

            try (final IgniteCache<AccountKey, Account> cache = ignite.getOrCreateCache("mainCache")) {

                Account account = cache.get(new AccountKey(4));
                System.out.println(account);
            }
        }
    }

    public void addManyAccounts1() {
        Ignition.setClientMode(true);

        try (Ignite ignite = Ignition.start("example-ignite.xml")) {

            try (final IgniteCache<AccountKey, Account> cache = ignite.getOrCreateCache("mainCache")) {
                for (int i = 1100; i < 2000; i++) {
                    cache.put(new AccountKey(i), new Account(i, "AAAAA", BigDecimal.valueOf(1000)));
                }
            }
        }
    }

    public void addManyAccounts2() {
        Ignition.setClientMode(true);
        try (Ignite ignite = Ignition.start("example-ignite.xml")) {

            try (final IgniteCache<AccountKey, Account> cache = ignite.getOrCreateCache("mainCache")) {
                for (int i = 2000; i < 3000; i++) {
                    cache.put(new AccountKey(i), new Account(i, "BBBBB", BigDecimal.valueOf(1000)));
                }
            }

        }
    }


    public void addManyAccounts3() {
        Ignition.setClientMode(true);
        try (Ignite ignite = Ignition.start("example-ignite.xml")) {

            try (final IgniteCache<AccountKey, Account> cache = ignite.getOrCreateCache("mainCache")) {
                for (int i = 3000; i < 4000; i++) {
                    cache.put(new AccountKey(i), new Account(i, "CCCCC", BigDecimal.valueOf(1000)));
                }
            }

        }
    }

}
