package com.amrusoft.valiev;

import com.amrusoft.valiev.model.Account;
import com.amrusoft.valiev.model.AccountKey;
import com.amrusoft.valiev.service.DataService;
import com.amrusoft.valiev.service.TransferService;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cluster.ClusterGroup;
import org.apache.ignite.lang.IgniteRunnable;

/**
 * Created by Айрат on 20.08.2016.
 */
public class IgniteRunner {


    public static void main(String[] args) {
        testSimpleTransaction();
//        testExistsValueWBHMode();
    }

    public static void testDataMode() {
        DataService dataService = new DataService();
        dataService.testAccounts();
    }

    public static void testComputeMode() {
        Ignition.setClientMode(true);
        try (Ignite ignite = Ignition.start("example-ignite.xml")) {

            ClusterGroup clusterGroup = ignite.cluster().forRemotes();
            ignite.compute().broadcast(new IgniteRunnable() {
                @Override
                public void run() {
                    System.out.println("Hello World");
                }
            });

        }
    }

    public static void testSimpleTransaction() {

//        try (Ignite ignite = Ignition.start("example-ignite.xml")) {
            Ignite ignite = Ignition.start("example-ignite.xml");
            try (final IgniteCache<AccountKey, Account> cache = ignite.getOrCreateCache("mainCache")) {

                TransferService transactionService = new TransferService(cache);
                Account a1 = cache.get(new AccountKey(1001));
                System.out.println("Credit: " + a1.toString());
                Account a2 = cache.get(new AccountKey(1002));
                System.out.println("Debit: " + a2.toString());
                transactionService.makeTransfer(a1, a2, 200);
                System.out.println("Complete");

            }

//        }
    }

    public static void testExistsValueWBHMode() {
        Ignition.setClientMode(true);
        try (Ignite ignite = Ignition.start("example-ignite.xml")) {
            try (final IgniteCache<AccountKey, Account> cache = ignite.getOrCreateCache("mainCache")) {
                Account account = cache.get(new AccountKey(1001));
                System.out.println(account);

            }

        }
    }


}
