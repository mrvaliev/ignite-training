package com.amrusoft.valiev;

import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;

/**
 * Created by Айрат on 22.08.2016.
 */
public class IgniteNodeStartup {

    public static void main(String[] args) {
        Ignite ignite = Ignition.start("example-ignite.xml");

    }
}
